(function(){
    angular
        .module('communityAPI')
        .service('questionService', questionService);

    questionService.$inject = ['$http', '$q', 'ENV', 'answersService', 'errorService'];

    function questionService($http, $q, ENV, answersService, errorService) {

        var questionEndpoint = ENV.apiEndpoint + 'questions';
        var audienceEndpoint = ENV.audienceEndpoint;
        var searchQuestionsEndpoint = ENV.apiEndpoint + 'search';

        return{
            getQuestions : getQuestions,
            getUnansweredQuestions : getUnansweredQuestions,
            getQuestionsByUser : getQuestionsByUser,
            searchQuestions : searchQuestions,
            getQuestionById : getQuestionById,
            getQuestionWithAnswers: getQuestionWithAnswers,
            deleteQuestion: deleteQuestion,
            editQuestion: editQuestion
        };

        /////////////////////////////

        /**
         * Get all questions sorted
         * @returns {*|Promise<U>}
         */
        function getQuestions(sortProp){
            var queryString = '?mode=';
            switch(sortProp){
                case "active":
                    queryString = queryString + sortProp;
                    break;
                default:
                    queryString = '';
            }

            return $http
                .get(questionEndpoint + audienceEndpoint + queryString)
                .then(onGetQuestionsSuccess)
                .catch(onGetQuestionsError);

            function onGetQuestionsSuccess(response){
                return response.data.content;
            }

            function onGetQuestionsError(response){
                return errorService.parseRejectPromise(response);
            }
        }


        function deleteQuestion(questionID){
            return $http
                .delete(questionEndpoint + '/' + questionID + audienceEndpoint)
                .then(onGetQuestionsSuccess)
                .catch(onGetQuestionsError);

            function onGetQuestionsSuccess(response){
                return response.data.content;
            }

            function onGetQuestionsError(response){
                return errorService.parseRejectPromise(response);
            }
        }


        function editQuestion(data, questionId){
            return $http
                .patch(questionEndpoint + '/' + questionId + audienceEndpoint, data)
                .then(onGetQuestionsSuccess)
                .catch(onGetQuestionsError);

            function onGetQuestionsSuccess(response){
                return response.data.content;
            }

            function onGetQuestionsError(response){
                return errorService.parseRejectPromise(response);
            }
        }

        /////////////////////////////

        /**
         * Get questions by user
         * @returns {*|Promise<U>}
         */
        function getQuestionsByUser(username){
            return $http
                .get(questionEndpoint + audienceEndpoint + '?mode=user&username=' + username)
                .then(onGetQuestionsByUserSuccess)
                .catch(onGetQuestionsByUserError);

            function onGetQuestionsByUserSuccess(response){
                return response.data.content;
            }

            function onGetQuestionsByUserError(response){
                return errorService.parseRejectPromise(response);
            }
        }


        /////////////////////////////

        /**
         * Get unanswered questions
         * @returns {*|Promise<U>}
         */
        function getUnansweredQuestions(){
            return $http
                .get(questionEndpoint + audienceEndpoint + '?mode=unanswered')
                .then(onGetUnansweredQuestionsSuccess)
                .catch(onGetUnansweredQuestionsError);

            function onGetUnansweredQuestionsSuccess(response){
                return response.data.content;
            }

            function onGetUnansweredQuestionsError(response){
                return errorService.parseRejectPromise(response);
            }
        }

        /////////////////////////////

        /**
         * Search questions
         * @returns {*|Promise<U>}
         */
        function searchQuestions(query){
            return $http
                .get(searchQuestionsEndpoint + audienceEndpoint + '?text=' + query)
                .then(onSearchQuestionsSuccess)
                .catch(onSearchQuestionsError);

            function onSearchQuestionsSuccess(response){
                return response.data.content;
            }

            function onSearchQuestionsError(response){
                return errorService.parseRejectPromise(response);
            }
        }



        /////////////////////////////

        /**
         * Gets question by given id
         * @param id, id of question to retrieve
         * @returns {*|Promise<U>}
         */
        function getQuestionById(id){
            return $http
                .get(questionEndpoint + '/' + id + audienceEndpoint)
                .then(onGetQuestionSuccess)
                .catch(onGetQuestionError);

            function onGetQuestionSuccess(response){
                return response.data.content;
            }

            function onGetQuestionError(response){
                return errorService.parseRejectPromise(response);
            }
        }


        /////////////////////////////

        /**
         * Get Question with its Answers.
         * Queue two request in an array and only execute "then" callback when
         * both are successful. This way we do not need extra synchronisation regarding two
         * asynchronous request.
         * @param questionID
         * @returns {*}
         */
        function getQuestionWithAnswers(questionID){
            var promises = [getQuestionById(questionID), answersService.getAnswers(questionID)];

            return $q
                .all(promises)
                .then(onGetQuestionWithAnswersSuccess)
                .catch(onGetQuestionWithAnswersError);

            function onGetQuestionWithAnswersSuccess(response){
                return response;
            }

            function onGetQuestionWithAnswersError(response){
                return errorService.parseRejectPromise(response);
            }
        }

    }
})();