describe('Question Service', function () {
    var questionService, httpBackend, answersService;

    beforeEach(module('communityAPI'));
    
    beforeEach(inject(function($injector) {
        questionService = $injector.get('questionService');
        answersService = $injector.get('answersService');
        httpBackend = $injector.get('$httpBackend');
    }));
    
    var response = {
        content: [
            {title:'Test1'},
            {title:'Test2'},
            {title:'Test3'}
        ]
    };

    var idResponse = {
        content:'Test Response'
    };

    var expectedMsg = 'We’re sorry. The server was unavailable. Please try again.';


    it("should successfully get all questions", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions')
            .respond(response);
        
        questionService.getQuestions()
            .then(function(questions) {
                expect(questions.length).toBe(3);
            });
        
        httpBackend.flush();
    });

    it("should execute catch block if request is invalid", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions')
            .respond(503, {});

        questionService.getQuestions()
            .then(function(questions) {
                return false;
            })
            .catch(function(response){
                expect(response).toBe(expectedMsg);
            });

        httpBackend.flush();
    });


    it("should get a question with an id", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2')
            .respond(idResponse);

        questionService.getQuestionById(2)
            .then(function(response){
                expect(response).toBe('Test Response' );
            });

        httpBackend.flush();
    });

    it("should get unanswered questions", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions?mode=unanswered')
            .respond(idResponse);

        questionService.getUnansweredQuestions()
            .then(function(response){
                expect(response).toBe('Test Response' );
            });

        httpBackend.flush();
    });

    it("should get questions for username = marius", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions?mode=user&username=marius')
            .respond(idResponse);

        questionService.getQuestionsByUser('marius')
            .then(function(response){
                expect(response).toBe('Test Response' );
            });

        httpBackend.flush();
    });

    it("should get questions that match the search term 'amazon'", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/search?text=amazon')
            .respond(idResponse);

        questionService.searchQuestions('amazon')
            .then(function(response){
                expect(response).toBe('Test Response' );
            });

        httpBackend.flush();
    });

    it("should execute catch block if request is invalid", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2')
            .respond(503, {});

        questionService.getQuestionById(2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBe(expectedMsg);
            });

        httpBackend.flush();
    });

    it("should get Question with its Answers", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2')
            .respond(idResponse);

        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(idResponse);

        questionService.getQuestionWithAnswers(2)
            .then(function(response){
                expect(response.length).toBe(2);
                expect(response[0]).toBe('Test Response');
                expect(response[1]).toBe('Test Response');
            });

        httpBackend.flush();
    });

    it("should execute catch block if server error occurs when getting Question with its Answers", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2')
            .respond(503, {});

        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(503, {});

        questionService.getQuestionWithAnswers(2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBe('');
            });

        httpBackend.flush();
    });

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });
    
});