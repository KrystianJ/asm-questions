(function(){
    angular
        .module('communityAPI')
        .service('errorService', errorService);

    errorService.$inject = ['$q'];

    function errorService($q) {

        return {
            parseRejectPromise: parseRejectPromise
        };

        function parseRejectPromise(reason){
            var msg = '';

            if(reason.status === 503){
                msg = 'We’re sorry. The server was unavailable. Please try again.';
            }
            else if(reason.status === 408){
                msg = 'We’re sorry. The server was unresponsive. Please try again.';
            }
            else if(reason.status === 500){
                msg = 'We’re sorry. There was a server error. Please try again.';
            }
            return $q.reject(msg);
        }

    }
})();