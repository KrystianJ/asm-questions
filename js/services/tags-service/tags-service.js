(function(){
    angular
        .module('communityAPI')
        .service('tagsService', tagsService);

    tagsService.$inject = ['$http'];

    function tagsService($http) {

        return{
            getTags : getTags
        };

        function getTags(){
            return $http.get('/js/services/tags-service/tags.json')
        }
    }
})();