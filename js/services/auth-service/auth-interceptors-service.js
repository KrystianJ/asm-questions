(function(){
    angular
        .module('communityAPI')
        .factory('authInterceptorsService', authInterceptorsService);

    authInterceptorsService.$inject = ['$location', '$q', 'ENV', 'jwtHelper', '$injector', '$cookies'];

    function authInterceptorsService($location, $q, ENV, jwtHelper, $injector, $cookies) {

        return{
            request : request,
            response : response,
            responseError: responseError
        };


        /**
         * Interceptor for every out-coming request.
         * Get jwt token from local storage and append it do 
         * Auth headers
         * @param config, HTTP config headers
         * @returns {config}, update HTTP config headers
         */
        function request(config) {
            config.headers = config.headers || {};


            if(config.url.indexOf(ENV.apiEndpoint) === 0
                || config.url.indexOf(ENV.refreshEndpoint) === 0
                || config.url.indexOf(ENV.authEndpoint) === 0) {

                localStorageService = $injector.get('localStorageService');

                var jwtToken;
                if($cookies.get('jwt')){
                    jwtToken = {};
                    jwtToken.token = $cookies.get('jwt');
                } else {
                    jwtToken = localStorageService.get('jwt');
                    if(!jwtToken){
                        // @TODO Fix jwt case
                        // window.location.href = ENV.campusUrl + 'logout';
                    }
                }

                // Check if token expired and refresh if necessary
                if (typeof config.skipIntercept === 'undefined'
                    && jwtToken != null
                    && jwtHelper.isTokenExpired(jwtToken.token)) {

                    var $http = $injector.get('$http');

                    return $http
                        .post(ENV.refreshEndpoint, '',
                            {
                                skipIntercept: true,
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                    'Accept': 'application/json'
                                }
                            }
                        )
                        .then(function (response) {
                            localStorageService.set('jwt', { token: response.data.token});

                            //set exp date for jwt cookie
                            var now = new Date();
                            now.setDate(now.getDate() + ENV.campusCookieDTL);

                            $cookies.remove('jwt');
                            $cookies.put('jwt', response.data.token, {expires: now, domain: ENV.campusCookieUrl});


                            jwtToken = {};
                            jwtToken.token = response.data.token;


                            config.headers.Authorization = 'Bearer ' + jwtToken.token;
                            return config;
                        })
                        .catch(function (response) {
                            return $q.reject(response);
                        });

                }

                if (jwtToken) {
                    config.headers.Authorization = 'Bearer ' + jwtToken.token;
                }
                else {
                    config.withCredentials = false;
                }
            }
            return config;
        }


        /**
         * Interceptor for every in-coming request.
         * Gets new token from response and set it 
         * in local storage, otherwise redirect to home.
         * @param response, API response
         */
        function response(response){
            return response;
        }

        function responseError(response){
            if(response.status === '401'){
                localStorageService.remove('jwt');
                window.location.href = ENV.campusUrl + 'logout';
            }

            return $q.reject(response);
        }

    }
})();