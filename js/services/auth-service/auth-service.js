(function(){
    angular
        .module('communityAPI')
        .factory('authService', authService);

    authService.$inject = ['$http', '$q', 'ENV', '$injector', '$cookies', 'currentUserService'];

    function authService($http, $q, ENV, $injector, $cookies, currentUserService) {

        return{
            login : login,
            logout : logout,
            refreshJwtToken : refreshJwtToken
        };


        /**
         * Refresh an expired JWT token and set it in the local storage
         * @param token
         */
        function refreshJwtToken() {

            return $http
                .post(ENV.refreshEndpoint, '',
                    {
                        skipIntercept: true,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Accept': 'application/json'
                        }
                    }
                )
                .then(onRefreshSuccess)
                .catch(onRefreshError);
        }


        /**
         * Sets JWT token in local storage
         * @param response
         */
        function onRefreshSuccess(response){
            var localStorageService = $injector.get('localStorageService');

            localStorageService.set('jwt', { token: response.data.token});

            return response.data.token;
        }

        function onRefreshError(response){
            return $q.reject(response);
        }

        /**
         * This is temp for now, rather than login to get the token
         * we will get it from cookies.
         * @param loginData, login information
         */
        function login(loginData) {

            var credentials = 'email=' + loginData.email + '&password=' + loginData.password;
            
            return $http
                    .post(ENV.authEndpoint, credentials,
                        {
                            skipIntercept: true,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Accept': 'application/json'

                            }
                        }
                    )
                    .then(onLoginSuccess)
                    .catch(onLoginError);

        }

        /**
         * Sets JWT token in local storage
         * @param response
         */
        function onLoginSuccess(response){
            var localStorageService = $injector.get('localStorageService');

            localStorageService.set('jwt', { token: response.data.token});
            localStorageService.set('username', response.data.username);  //TODO: Temp solution to provide access to current user's username
            $cookies.put('jwt', response.data.token);

            currentUserService.setCurrentUser(
                response.data.member_id,
                response.data.username,
                response.data.firstname,
                response.data.lastname,
                response.data.image
            );

            return response;
        }

        function onLoginError(response){
            return $q.reject(response);
        }

        /**
         * This is temp for now, clears the cookies
         */
        function logout() {
            currentUserService.removeCurrentUser();
            var cookies = $cookies.getAll();
            angular.forEach(cookies, function (v, k) {
                $cookies.remove(k);
            });
        }

    }
})();