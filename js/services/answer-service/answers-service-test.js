describe('Answers Service', function () {
    var answersService, httpBackend;

    beforeEach(module('communityAPI'));

    beforeEach(inject(function($injector) {
        answersService = $injector.get('answersService');
        httpBackend = $injector.get('$httpBackend');
    }));

    var response = {
        content: [
            {title:'Test1'},
            {title:'Test2'},
            {title:'Test3'}
        ]
    };

    var idResponse = {
        content: {
            msg:'Test1',
            author:'Nick',
            created_at:'1234567'
        }
    };

    var postResponse = {
        content: {
            id: 10
        }
    };
    
    var expectedMsg = 'We’re sorry. The server was unavailable. Please try again.';


    it("should get answers for a given question", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(response);
        
        answersService
            .getAnswers(2)
            .then(function(response){
                var tmp = response.map(function(key, value){
                    return key;
                });

                expect(tmp.length).toBe(3);
            });

        httpBackend.flush();
    });

    it("should try to get answers and execute a catch block", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(503, {});

        answersService
            .getAnswers(2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBe(expectedMsg);
            });

        httpBackend.flush();
    });

    it('should get answer by id', function(){
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/2')
            .respond(idResponse);
    
        answersService
            .getAnswerById(2,2)
            .then(function(response){
                expect(response).toBeDefined();
                expect(response.msg).toBe('Test1');
                expect(response.author).toBe('Nick');
            });
    
        httpBackend.flush();
    });
    
    it("should try to get answers by id and execute a catch block", function () {
        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/2')
            .respond(503, {});
    
        answersService
            .getAnswerById(2,2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBe(expectedMsg);
            });
    
        httpBackend.flush();
    });
    
    it('should post an answer', function(){
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(postResponse);
    
        answersService
            .postAnswer(2)
            .then(function(response){
                expect(response).toBe(10);
            });
    
        httpBackend.flush();
    });
    
    it('should try to post an answer and execute catch block', function(){
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(503, {});
    
        answersService
            .postAnswer(2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBe(expectedMsg);
            });
    
        httpBackend.flush();
    });

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

});