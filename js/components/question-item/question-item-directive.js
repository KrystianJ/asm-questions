(function(){

    angular
        .module('component-question-item')
        .directive('questionItem', questionItem);

    questionItem.$inject = ['$location', 'currentUserService', '$rootScope'];

    function questionItem($location, currentUserService, $rootScope){
        return {
            restrict: 'E',
            replace: true,
            scope: {
                question: '=',
                deleteQuestion: '&'
            },
            templateUrl : 'js/components/question-item/question-item.html',
            link: function (scope){

                scope.goToQuestion = goToQuestion;
                scope.goToUser = goToUser;
                scope.tagClick = onTagClick;
                scope.searchTag = searchTag;
                scope.delQuestion = delQuetion;
                scope.editQuestion = editQuestion;


                scope.is_mentor = $rootScope.is_mentor;

                function goToQuestion(question){
                    $location.search({});
                    $location.path('/questions/' + question.id);

                }

                function goToUser(user){
                    $location.search({});
                    $location.path('/profile/' + user.username);
                }

                function onTagClick(e){
                    // We dont want the # symbol so we strip it here
                    var tag = e.target.innerText.substring(1);
                    $location.url('/questions/search?query=' + tag);
                }

                function searchTag(tag){
                    $location.url('/questions/search?query=' + tag);
                }

                function delQuetion(){
                    // Delegate deleting to the parent scope
                    scope.deleteQuestion({question:scope.question})
                }

                function editQuestion(){
                    $location.url('/questions/' + scope.question.id + '/edit');
                }
            }
        };
    }

})();