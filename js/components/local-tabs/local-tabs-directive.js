(function(){

    angular
        .module('component-local-tabs')
        .directive('localtabs', localTabs);

    function localTabs(){
        return {
            restrict: 'E',
            scope: {
                info: '='
            },
            templateUrl : 'js/components/local-tabs/local-tabs.html',
            link: function (scope){
                scope.heading = '';
                scope.tabItems = [];
                scope.$watch('info', function(data){
                    if(data){
                        scope.heading = data.heading;
                        scope.tabItems = data.items;
                    }
                });
            }
        };
    }

})();