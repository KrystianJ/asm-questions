(function(){
    angular
        .module('component-footer')
        .directive('footer', footer);

    footer.$inject = ['ENV'];

    function footer(ENV){
        return {
            restrict: 'AE',
            templateUrl : 'js/components/footer/footer.html',
            link: function (scope){


                scope.termsUrl = ENV.campusUrl + 'terms';
                scope.privacyUrl = ENV.campusUrl + 'privacy';
                scope.dashboardUrl = ENV.campusUrl + 'dashboard'
            }
        };
    }    
})();