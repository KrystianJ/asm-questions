describe('Answers Controller', function () {

    beforeEach(module('answers'));
    beforeEach(module('communityAPI'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('areahashtag'));

    var scope,
        vm,
        $q,
        deferred,
        stateParams,
        httpBackend;

    var idResponse = {
        content: {
            msg:'Test1',
            author:'Nick',
            createdAt:'1234567'
        }
    };

    var postResponse = {
        content: {
            id: 10
        }
    };

    beforeEach(inject(function (_$rootScope_,$controller, _$q_, tagsService,
                                $uibModal, questionService, answersService, $httpBackend, votingService) {
        scope = _$rootScope_.$new();
        $q = _$q_;
        stateParams = {QuestionId: 2};
        deferred = _$q_.defer();
        httpBackend = $httpBackend;

        // Use a Jasmine Spy for mocking
        spyOn(tagsService, 'getTags').and.returnValue(deferred.promise);
        spyOn(questionService, 'getQuestionWithAnswers').and.returnValue(deferred.promise);


        // Init controller with mocked dependencies
        vm = $controller('answersCtrl', {
            $scope: scope,
            askQuestionService: questionService,
            tagsService: tagsService,
            answersService: answersService,
            votingService: votingService,
            $stateParams: stateParams
        });

    }));

    it('should have initialize scope vars correctly', function() {
        expect(vm).toBeDefined();
        expect(vm.answerMsg).toBe('');
        expect(vm.answerMaxLength).toBe(10000);
    });

    it('should cancel posting an answer', function() {
        vm.cancel();
        scope.$apply();
        expect(vm.expandAnswerBox).toBe(false);
    });

    it('should expand posting answer box', function() {
        vm.showAnswerBox();
        scope.$apply();
        expect(vm.expandAnswerBox).toBe(true);
    });

    it('should post an answer', function() {

        httpBackend
            .when('GET', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/10')
            .respond(idResponse);

        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers')
            .respond(postResponse);

        var answer = {
            id: 1,
            text: 'Test'
        };

        vm.answers = [answer];
        scope.$apply();


        vm.postAnswer();
        vm.answerMsg = 'Test Answer';
        scope.$apply();
        httpBackend.flush();

        expect(vm.loading).toBe(false);
        expect(vm.expandAnswerBox).toBe(false);
        expect(vm.answerMsg).toBe('');
        expect(vm.answers.length).toBe(2);
        expect(vm.answers[1].shouldScroll).toBe(true);

    });

    it('should not post an answer if current state is loading', function() {
        var answer = {
            id: 1,
            votes : {
                count : 1,
                hasCurrentUserVoted: false
            }
        };

        vm.answers = [answer];
        vm.loading = true;
        scope.$apply();


        vm.postAnswer();
        scope.$apply();

        expect(vm.answers.length).toBe(1);

    });

    it('should show inline error if blur event was fires ', function() {
        vm.onBlur();
        scope.$apply();
        expect(vm.showInlineError).toBe(true);
    });

    it('should vote up an answer if current user has not voted', function() {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/1/vote')
            .respond({});

        var answer = {
            id: 1,
            votes : {
                count : 0,
                hasCurrentUserVoted: false
            }
        };

        vm.answers = [answer];
        scope.$apply();

        vm.vote(answer);
        scope.$apply();
        httpBackend.flush();

        expect(vm.answers[0].votes.count).toBeDefined();
        expect(vm.answers[0].votes.count).toBe(1);
        expect(vm.answers[0].votes.hasCurrentUserVoted).toBe(true);
    });

    it('should un vote an answer if currently logged in user has voted', function() {

        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/1/vote')
            .respond({});

        var answer = {
            id: 1,
            votes : {
                count : 1,
                hasCurrentUserVoted: true
            }
        };

        vm.answers = [answer];
        scope.$apply();

        vm.vote(answer);
        scope.$apply();

        expect(vm.answers[0].votes.count).toBeDefined();
        expect(vm.answers[0].votes.count).toBe(0);
        expect(vm.answers[0].votes.hasCurrentUserVoted).toBe(false);
    });

    it('should have correct count for votes after multiply votes and un-votes', function() {

        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/1/vote')
            .respond({});

        var answer = {
            id: 1,
            votes : {
                count : 0,
                hasCurrentUserVoted: false
            }
        };

        vm.answers = [answer];
        scope.$apply();

        vm.vote(answer);
        scope.$apply();
        httpBackend.flush();

        expect(vm.answers[0].votes.count).toBe(1);
        expect(vm.answers[0].votes.hasCurrentUserVoted).toBe(true);

        vm.vote(answer);
        scope.$apply();
        httpBackend.flush();


        expect(vm.answers[0].votes.count).toBe(0);
        expect(vm.answers[0].votes.hasCurrentUserVoted).toBe(false);

        vm.vote(answer);
        scope.$apply();
        httpBackend.flush();

        expect(vm.answers[0].votes.count).toBeDefined();
        expect(vm.answers[0].votes.count).toBe(1);
        expect(vm.answers[0].votes.hasCurrentUserVoted).toBe(true);
    });


    it('should lock voting up an answer', function() {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/1/vote')
            .respond({});

        var answer = {
            id: 1,
            votes : {
                count : 0,
                hasCurrentUserVoted: false
            }
        };

        vm.answers = [answer];
        scope.$apply();

        vm.vote(answer);
        scope.$apply();


        expect(vm.answers[0].votes.locked).toBe(true);
    });

    it('should unlock voting an answer if API call is successful', function() {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/1/vote')
            .respond({});

        var answer = {
            id: 1,
            votes : {
                count : 0,
                hasCurrentUserVoted: false
            }
        };

        vm.answers = [answer];
        scope.$apply();

        vm.vote(answer);
        scope.$apply();
        expect(vm.answers[0].votes.locked).toBe(true);

        httpBackend.flush();
        expect(vm.answers[0].votes.locked).toBe(false);

    });

    it('should accept an answer', function() {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/1/accept')
            .respond({});

        var answer = {
            id: 1,
            votes : {
                count : 0,
                hasCurrentUserVoted: false
            }
        };
        var question = {
            id: 1,
            hasAcceptedAnswer: 0
        };

        vm.question= question;
        vm.answers = [answer];
        scope.$apply();

        vm.acceptAnswer(answer);
        httpBackend.flush();

        expect(vm.answers[0].accepted).toBe(1);
        expect(vm.question.hasAcceptedAnswer).toBe(1);

    });

    it('should be able to accept an answer if question has not been solved yet', function() {
        vm.question = {
            currentUserCanAcceptSolution: 1,
            hasAcceptedAnswer: 0
        };

        var result = vm.canUserAcceptAnswer();
        expect(result).toBe(true);
    });

    it('should not be able to accept an answer if logged in user did not create the question', function() {
        vm.question = {
            currentUserCanAcceptSolution: 0,
            hasAcceptedAnswer: 0
        };

        var result = vm.canUserAcceptAnswer();
        expect(result).toBe(false);
    });

    it('should not be able to accept an answer if question has ben solved previously', function() {
        vm.question = {
            currentUserCanAcceptSolution: 0,
            hasAcceptedAnswer: 1
        };

        var result = vm.canUserAcceptAnswer();
        expect(result).toBe(false);
    });


});
