(function(){
    /*global angular*/

    angular
        .module('ask-question')
        .controller('askCtrl', AskQuestion);

    AskQuestion.$inject = ['$uibModal', '$rootScope', 'askQuestionService', '$location', 'tagsService' ,'$scope'];

    function AskQuestion($uibModal, $rootScope , askQuestionService, $location, tagsService, $scope) {
        'use strict';

        var vm = this,
            hasOpenedWarningModal= false,
            minTitleLength = 10,
            softTagLimit = 5,
            maxNumberOfTags = 20;

        // Scope vars 
        vm.questionTitle = '';
        vm.questionDesc = '';
        vm.addedTagsDescription = [];
        vm.addedTagsTitle = [];
        vm.totalTagsCount = [];
        vm.titleError = false;
        vm.loading = false;
        vm.showHelp = false;
        vm.promptText = 'You should add #tags to your question title or details. ' +
            'Tags are used to help others find and answer your question. ' +
            'Choose from the suggested tags list, or type your own.';
        vm.showTagsErrorAlert = false;
        vm.showTagsWarningAlert = false;

        // Scope functions
        vm.postAnswer = postAnswer;
        vm.addTagsDescription = addTagsDescription;
        vm.addTagsTitle = addTagsTitle;
        vm.openHelpVideo = openHelpVideo;
        vm.suggestedTag = suggestedTag;
        vm.setFieldFocus = setFieldFocus;
        vm.cancel = cancel;
        vm.toggleHelp = toggleHelp;
        vm.closePrompt = closePrompt;

        vm.actionMenuList= {
            questions: {
                display: true,
                active: false,
                link: 'questions',
                text: 'Questions'
            }
        };

        init();

        function init(){
            tagsService
                .getTags()
                .then(function(data){
                    vm.suggestedTags = data.data.tags;
                });
            startWatchingTags();
            vm.showPrompt = !$rootScope.hidePrompt;
        }

        function postAnswer(){
            
            if(vm.loading){
                return;
            }

            vm.totalTagsCount = vm.addedTagsTitle.length + vm.addedTagsDescription.length;

            // Check for required amount of letters 
            if(vm.questionTitle.length < minTitleLength){
                vm.titleError = true;
                $rootScope.$broadcast('focus:on');
                return false;
            }

            // Not more than 20 tags can be added 
            if(vm.totalTagsCount >= maxNumberOfTags){
                vm.showTagsErrorAlert = true;
                return false;
            }

            vm.loading = true;

            var data = {
                text: vm.questionTitle,
                description : vm.questionDesc
            };

            askQuestionService
                .postQuestion(data)
                .then(function(id){
                    $location.path('/questions/' + id);
                })
                .catch(function(reason){
                    openErrorModal();
                    vm.loading = false;
                });
        }

        function suggestedTag(tag){
            vm.questionDesc = vm.questionDesc + ' ' + tag;
        }

        // Add Tags from description 
        function addTagsDescription(tags){
            vm.addedTagsDescription = tags;
        }

        // Add Tags from title
        function addTagsTitle(tags){
            vm.addedTagsTitle = tags;
        }

        function setFieldFocus(){
            vm.focusField = true;
            $rootScope.hidePrompt = true;
            vm.showPrompt = false;
        }

        function cancel(){
            $location.path('/');
        }

        function toggleHelp(){
            vm.showHelp = !vm.showHelp;
        }
        
        function closePrompt(){
            $rootScope.hidePrompt = true;
            vm.showPrompt = false;
        }

        /**
         * Watch the collection of tags from title and description of question.
         * If the combined legth of two arrays is greater than the soft tag limit
         * show a modal.
         */
        function startWatchingTags(){
            $scope.$watchGroup(['vm.addedTagsTitle', 'vm.addedTagsDescription'],
                function(tagsArray){

                    var totalTagsLength = 0;
                    // Make sure the arrays are not undefined 
                    if(tagsArray[0] && tagsArray[1]){
                        totalTagsLength = tagsArray[0].length + tagsArray[1].length;
                    }

                    if(totalTagsLength >= softTagLimit && !hasOpenedWarningModal){
                        hasOpenedWarningModal = true;
                        vm.showTagsWarningAlert = true;
                    }
                }
            );
        }

        function openHelpVideo() {
            mixpanel.track('Video play', {'Sponsored': true, 'Video Location': 'Ask a question'});
            
            $uibModal.open({
                templateUrl: '/js/helpers/video-modal/video.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl'
            });
        }

        function openErrorModal(){
            $uibModal.open({
                templateUrl: '/js/helpers/error-modal/error-modal.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl'
            });
        }

    }
})();