(function(){
    /*global angular*/

    angular
        .module('edit-answer')
        .controller('editAnswerController', EditAnswerController);

    EditAnswerController.$inject = ['answersService', '$stateParams', '$location'];

    function EditAnswerController(answersService, $stateParams, $location) {
        'use strict';

        var vm = this,
            questionId,
            answerId;

        vm.updateAnswer = updateAnswer;
        vm.cancel = cancel;

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };

        init();

        function init(){
            questionId = $stateParams.QuestionId;
            answerId = $stateParams.AnswerId;

            answersService
                .getAnswerById(questionId, answerId)
                .then(function(response){
                    vm.answer = response;
                })
        }

        function updateAnswer(){
            vm.loading = true;
            answersService
                .updateAnswer(questionId, answerId, vm.answer.text)
                .then(function(){
                    $location.url('/questions/' + questionId);
                })
                .catch(function(response){
                    console.log('error', response);
                    vm.loading = false;
                })
        }

        function cancel(){
            $location.url('/questions/' + questionId);
        }



    }
})();