(function(){
    /*global angular*/
    angular
        .module('community')
        .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['authService', '$location', '$cookies'];

    function homeCtrl(authService,$location, $cookies) {

        var vm = this;

        vm.selectLoginUSer = selectLoginUSer;

        var listOfUsers = [
            {
                email:'marius@amazing.com',
                password:'128064'
            },
            {
                email: 'chris@amazing.com',
                password: 'Amazing12341!'
            },
            {
                email:'gary@amazing.com',
                password:'@mazing18'
            },
            {
                email:'marius@amazing.com',
                password:'128064'
            },
            {
                email:'krystian@amazing.com',
                password:'qazwsxedc'
            }
        ];

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'home',
                text: 'Questions'
            },
            ask: {
                display: true,
                active: false,
                special: true,
                link: 'ask',
                text: 'Ask a Question'
            }
        };


        function selectLoginUSer(index){
            var newUser = listOfUsers[index];
            authService
                .login(newUser)
                .then(function(data){
                    // This is to mock member info the the cookies, in prod this is supplied as cookies from Web campus
                    $cookies.put('member_info', '%7B%22firstname%22%3A%22Marius%22%2C%22lastname%22%3A%22%22%2C%22image%22%3A%22https%3A//s3.amazonaws.com/amazing.avatars/185094-771593358es-3.jpg%22%2C%22username%22%3A%22marius%22%7D');
                    $location.path('/questions');
                });
        }

    }
})();