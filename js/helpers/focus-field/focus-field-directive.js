(function(){
    angular
        .module('focus',[])
        .directive('focusField', focusField);

    function focusField(){
        return function(scope, elem) {
            scope.$on('focus:on', function() {
                elem[0].focus();
            });
        };
    }
})();