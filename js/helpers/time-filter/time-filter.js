(function () {
    angular
        .module('timeFilter',[])
        .filter('timeFilter', timeFilter);
    
    function timeFilter() {
        return function (unixTimestamp) {

            if(!unixTimestamp){
                return '';
            }

            // Time in ms
            var minute = 60 * 1000,
                hour = 60 * minute,
                oneDay = 24 * hour,
                twoDays = 2 * oneDay,
                result;

            var timestamp = unixTimestamp * 1000;
            var now = new Date();

            var timeDiff = now - timestamp;

            if(timeDiff < minute){
                result = parseInt((timeDiff / 1000)%60) + 's';
            }
            else if(timeDiff < hour){
                result = parseInt((timeDiff/minute)%60) + 'm';
            }
            else if(timeDiff< oneDay){
                result = parseInt((timeDiff/hour)%24) + 'h';
            }
            else if(timeDiff < twoDays){
                result = 'yesterday';
            }
            else{
                result = moment.unix(unixTimestamp).format('MMM DD, YYYY');
            }

            return result;
        };
    }
    
}());
