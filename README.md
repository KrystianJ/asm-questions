#This Readme describes the Community project

##Installation

Fork this repo and clone it locally. In the root of the project run the fallowing commands: 

```
npm install
```

This will install build dependencies for the project like grunt etc. 

```
bower install
```

This will install project dependencies like angular etc. 

To run the project, open it in WebStorme and open the index.html file, go to right top corner and open in Chrome. This will 
start local server. Alternatively run php server:

```
php -S 0.0.0.0:8888
```


##Development
In DEV ENV all JS files are loaded in index.html for debugging purposes. Each ENV requires running of correct grunt task to populate 
correct config files.

For DEV run:

```
grunt dev-build
```

For QA run:

```
grunt qa-build
```

For Production run:

```
grunt production-build
```

The production build also minifies JS files and replaces normal JS files with min version.

##CSS
For DEV all CSS is written in LESS. To watch for changes and automaticly compile all less files run:

```
grunt
```

The default taks points to LESS and WATCH tasks

##Testing
To run unit tests execute:

```
grunt unit-tests
```

This task also lints the JS code. 