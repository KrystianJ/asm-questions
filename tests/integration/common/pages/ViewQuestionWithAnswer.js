/**
 * Created by garyfox on 26/08/2016.
 */
/*jshint -W109 */
var helpers = require('../helpers.js');
var _ = require('lodash');
var data = require('../data.json');
var viewQuestionWithAnswerObjects = require('../objects/ViewQuestionWithAnswerObjects.js');

var ViewQuestionWithAnswer = function () {
    this.verifyOnQuestionWithAnswerPage = function () {
        helpers.checkElementExists(viewQuestionWithAnswerObjects.containerQuestionsWithAnswers(), 10);
        helpers.checkElementExists(viewQuestionWithAnswerObjects.divQuestionHeader(), 10);
        helpers.checkElementExists(viewQuestionWithAnswerObjects.divQuestionDetail(), 10);
        helpers.getQuestionIDFromUrl().then(function (res) {
            browser.params.questionID = parseInt(res);
        });
        return this;
    };

    this.postNewAnswer = function () {
        var time = Math.floor(Date.now() / 1000);
        browser.params.answerTime = time;
        helpers.checkElementExistAndClick(viewQuestionWithAnswerObjects.btnShowAnswerBox(), 10);
        helpers.checkElementExistAndSendKeys(data.answerText + time, viewQuestionWithAnswerObjects.txtAreaPostAnswer(), 10);
        helpers.checkElementExistAndClick(viewQuestionWithAnswerObjects.btnPostAnswer(), 10);
        return this;
    };

    this.verifyAnswerHasPosted = function (user) {
        helpers.checkElementExists(viewQuestionWithAnswerObjects.txtTimeStampFromAnswer(browser.params.answerTime), 10);
        helpers.checkElementExistsAndGetText(viewQuestionWithAnswerObjects.txtAvatarName(browser.params.answerTime), 10).then(
            function (text) {
                expect(text).toEqual('@' + user.name);
            });
        return this;
    };
    this.voteUpParticularAnswer = function (timestamp) {
        var currentVote;
        helpers.checkElementExistsAndGetText(viewQuestionWithAnswerObjects.txtNumberVotes(timestamp), 5)
            .then(function (text) {
                currentVote = parseInt(text);
                helpers.checkElementExistAndClick(viewQuestionWithAnswerObjects.btnVoteUp(timestamp),5);
                currentVote++;
                browser.sleep(2000).then(function () {
                    helpers.checkElementExists(viewQuestionWithAnswerObjects.txtNumberVotesWithNumber(timestamp, currentVote),5);
                });

            });
        return this;
    };
    this.voteDownParticularAnswer = function (timestamp) {
        var currentVote;
        helpers.checkElementExistsAndGetText(viewQuestionWithAnswerObjects.txtNumberVotes(timestamp), 5)
            .then(function (text) {
                currentVote = parseInt(text);
                helpers.checkElementExistAndClick(viewQuestionWithAnswerObjects.btnVoteUp(timestamp),5);
                currentVote--;
                helpers.checkElementExists(viewQuestionWithAnswerObjects.txtNumberVotesWithNumber(timestamp, currentVote),5);
            });
        return this;
    };

    this.selectAcceptedAnswer = function (timestamp) {
        helpers.getAllElements(viewQuestionWithAnswerObjects.btnGetAllAcceptedAnswers()).then(function (items) {
            expect(items.length).toEqual(browser.params.timestamps.length);
            helpers.checkElementExistAndClick(viewQuestionWithAnswerObjects.btnSelectParticularAcceptedAnswer(timestamp),5);
            helpers.checkElementExists(viewQuestionWithAnswerObjects.divAcceptedAnswerDivider(timestamp),5);
            helpers.getAllElements(viewQuestionWithAnswerObjects.btnGetAllAcceptedAnswers()).then(function (items) {
                expect(items.length).toEqual(0);
            });
        });
        return this;
    };

    this.verifyNoSelectedAnswerIsPresent = function () {
        helpers.getAllElements(viewQuestionWithAnswerObjects.btnGetAllAcceptedAnswers()).then(function (items) {
            expect(items.length).toEqual(0);
        });
        return this;
    };

};
module.exports = new ViewQuestionWithAnswer();