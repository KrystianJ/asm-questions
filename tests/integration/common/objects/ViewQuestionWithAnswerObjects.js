/**
 * Created by garyfox on 26/08/2016.
 */
/*jshint -W109 */
var ViewQuestionWithAnswerObject = function () {

    this.divQuestionHeader = function () {
        return by.xpath("//p[contains(@class,'question-title')]");
    };
    this.divQuestionDetail = function () {
        return by.xpath("//p[contains(@class,'question-description')]");
    };
    this.containerQuestionsWithAnswers = function () {
        return by.xpath("//div[contains(@class,'container question-answers')]");
    };
    this.btnPostAnswer = function () {
        return by.xpath("//button[@ng-click='vm.postAnswer()']");
    };
    this.txtAreaPostAnswer = function () {
        return by.xpath("//textarea[@smart-text-area]");
    };
    this.btnShowAnswerBox = function () {
        return by.xpath("//button[@ng-click='vm.showAnswerBox()']");
    };
    this.txtTimeStampFromAnswer = function (timestamp) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]");
    };
    this.txtAvatarName = function (timestamp) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]" +
            "/ancestor::div[contains(@ng-repeat,'answer in')]//a[@class='user-link ng-binding']");
    };
    this.btnVoteUp = function (timestamp) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]" +
            "/ancestor::div[@class='row answer-box divider']//a[contains(@class,'vote-up')]");
    };
    this.txtNumberVotes = function (timestamp) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]" +
            "/ancestor::div[@class='row answer-box divider']//span[contains(@class,'vote-count')]");
    };
    this.txtNumberVotesWithNumber = function (timestamp, number) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]/ancestor::div[@class='row answer-box divider']" +
            "//span[contains(@class,'vote-count')and text()='" + number + "']");
    };
    this.btnSelectParticularAcceptedAnswer = function (timestamp) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]/ancestor::div[@class='row']" +
            "//button[@class='com-btn com-btn--accept']");
    };
    this.btnGetAllAcceptedAnswers = function () {
        return by.xpath("//button[@class='com-btn com-btn--accept']");
    };
    this.divAcceptedAnswerDivider = function (timestamp) {
        return by.xpath("//span[@class='ng-scope' and contains(.,'" + timestamp + "')]" +
            "/ancestor::div[@class='row answer-box divider selected--answer']");
    };
};
module.exports = new ViewQuestionWithAnswerObject();