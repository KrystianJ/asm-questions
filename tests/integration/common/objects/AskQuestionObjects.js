/**
 * Created by garyfox on 26/08/2016.
 */
/*jshint -W109 */
var AskQuestionObject = function () {

    this.divVideoModal = function () {
        return by.xpath("//body[@class='ng-scope modal-open']");
    };
    this.txtQuestionHeader = function () {
        return by.xpath("//textarea[contains(@class,'mention-question-title')]");
    };
    this.txtQuestionDetail = function () {
        return by.xpath("//textarea[contains(@class,'mention-question-detail')]");
    };
    this.btnPostQuestion = function () {
        return by.xpath("//button[@ng-click='vm.postAnswer()']");
    };
    this.divBorderError = function () {
        return by.xpath("//textarea[contains(@class,'mention-question-title') and contains(@class,'input-error')]");
    };
    this.txtMinCharactersError = function () {
        return by.xpath("//span[@class='error-text ng-scope']");
    };
    this.modalError = function () {
        return by.xpath("//div[@class='modal-error ng-scope']");
    };
    this.txtInline20Tags = function(){
        return by.xpath("//div[@ng-if='vm.showTagsErrorAlert']");
    };
    this.txtInline5Tags = function(){
        return by.xpath("//div[@ng-if='vm.showTagsWarningAlert']");
    };
    this.btnCloseModal = function () {
        return by.xpath("//a[@ng-click='$ctrl.cancel()']");
    };
    this.btnCloseModalTagsAdded = function () {
     return by.xpath("//h3[contains(.,'Tags Added')]/ancestor::div[contains(@class,'modal-success')]//a[@ng-click]");
    };
    this.pageBody = function () {
        return by.xpath("//body[@ng-app]");
    };

};
module.exports = new AskQuestionObject();