/**
 * Created by garyfox on 12/10/2016.
 */
/*jshint -W089 */
var communityHome = require('../pages/CommunityHome.js');
var communityHomeShared = require('../../common/pages/CommunityHome');
var helpers = require('../../common/helpers.js');
var data = require('../../common/data.json');
var logger = require('../../common/log.js');
describe('Ask A Question Functionality', function () {
    var testParams = [data.users.userA, data.users.userB];
    var api = require('../../common/api.js')(browser.params.apiDetails.apiHost);
    beforeEach(function () {
        browser.get(browser.baseUrl);
    });
    it('Post Successful Answer Through New Question', function () {
        communityHomeShared
            .selectUser(testParams[0])
            .deepLinkToAskQuestion()
            .askSuccessfulQuestion(data.successfulQuestionTitle, data.successfulQuestionDetail)
            .verifyOnQuestionWithAnswerPage().postNewAnswer().verifyAnswerHasPosted(data.users.userA);

    });
    for (var currentParam = 0; currentParam < testParams.length; currentParam++) {
        (function (testSpec) {
            it('Post Successful Answer', function () {
                communityHomeShared.selectUser(testSpec).deepLinkToAnswer('3').postNewAnswer().verifyAnswerHasPosted(testSpec);
            });
        })(testParams[currentParam]);
    }

    afterEach(function () {

        if (browser.params.questionID !== undefined && browser.params.questionID !== 0) {
            var dbSettings = require('../../common/dbqueries.js')(browser.params.dbDetails);
            var connection = dbSettings.connect();
            helpers.removeQuestionById(dbSettings, browser.params.questionID);
            browser.params.questionID = 0;
            if (browser.params.answerTime !== undefined && browser.params.answerTime !== 0) {
                helpers.removeAnswerByTimeStamp(dbSettings, browser.params.answerTime);
                browser.params.time = 0;
            }
            browser.sleep(2000).then(function () {
                logger.log(data.loggingLevel.INFO, 'Done Deleting DB Entries');
                connection.end();
            });
        }

    });
});

