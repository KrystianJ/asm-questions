/**
 * Created by garyfox on 09/11/2016.
 */
/*jshint -W089 */
var communityHome = require('../pages/CommunityHome.js');
var communityHomeShared = require('../../common/pages/CommunityHome');
var helpers = require('../../common/helpers.js');
var logger = require('../../common/log.js');
var data = require('../../common/data.json');
describe('Ask A Question Functionality', function () {
    var testParams = [data.users.userA, data.users.userB];
    var api = require('../../common/api.js')(browser.params.apiDetails.apiHost);
    beforeEach(function () {
        browser.get(browser.baseUrl);
        api.postMultipleAnswersApi();
    });
    it('Voting Up And Down Successfully', function () {
        browser.sleep(6000).then(function () {
            logger.log(data.loggingLevel.INFO, 'Done Waiting For Api Calls');
            communityHomeShared
                .deepLinkToAnswer(browser.params.questionID)
                .verifyOnQuestionWithAnswerPage()
                .voteUpParticularAnswer(browser.params.timestamps[1])
                .voteDownParticularAnswer(browser.params.timestamps[1]);

        });

    });
    it('Select Accepted Answer', function () {
        browser.sleep(6000).then(function () {
            logger.log(data.loggingLevel.INFO, 'Done Waiting For Api Calls');
            communityHomeShared
                .deepLinkToAnswer(browser.params.questionID)
                .verifyOnQuestionWithAnswerPage()
                .selectAcceptedAnswer(browser.params.timestamps[1]);
        });

    });

    it('Cannot Select Accepted Answer', function () {
        browser.sleep(6000).then(function () {
            logger.log(data.loggingLevel.INFO, 'Done Waiting For Api Calls');
            communityHomeShared
                .selectUser(testParams[1])
                .deepLinkToAnswer(browser.params.questionID)
                .verifyOnQuestionWithAnswerPage()
                .verifyNoSelectedAnswerIsPresent();

        });

    });

});

afterEach(function () {
    if (browser.params.questionID !== undefined && browser.params.questionID !== 0) {
        var dbSettings = require('../../common/dbqueries.js')(browser.params.dbDetails);
        var connection = dbSettings.connect();
        helpers.removeQuestionById(connection, browser.params.questionID);
        browser.params.questionID = 0;
        if (browser.params.answerIds !== undefined && browser.params.answerIds.length > 0) {
            browser.params.answerIds.forEach(function (id) {
                helpers.removeAnswerById(connection, id);
            });
            browser.params.answerIds = [];
            browser.params.timestamps = [];
        }
        browser.sleep(2000).then(function () {
            logger.log(data.loggingLevel.INFO, 'Done Deleting DB Entries');
            connection.end();
        });
    }
});

