/**
 * Created by garyfox on 13/09/2016.
 */

var helpers = require('../../common/helpers.js');
var communityPageObjectsMobile = require('../objects/CommunityHomeObjects.js');
var communityPageObjects = require('../../common/objects/CommunityHomeObjects.js');

var CommunityHomeMobile = function () {

    this.clickAskAQuestionMobile = function () {
        helpers.checkElementExistAndClick(communityPageObjectsMobile.burgerNavCommunity(), 5);
        helpers.checkElementExistAndClick(communityPageObjects.lnkAskAQuestion(),5);
        return require('../../common/pages/AskQuestion.js');
    };

};
module.exports = new CommunityHomeMobile();