/**
 * Created by garyfox on 22/08/2016.
 */
var helpers = require('../../common/helpers.js');
var communityPageObjects = require('../../common/objects/CommunityHomeObjects.js');

var CommunityHome = function () {
    this.verifyOnCommunityHome = function () {
        helpers.checkElementExists(communityPageObjects.divFooter(), 1);
        helpers.checkElementExists(communityPageObjects.txtSearchQuestion(), 5);
        return this;


    };
    this.searchQuestion = function (text) {
        helpers.checkElementExistAndSendKeys(text, communityPageObjects.txtSearchQuestion(), 5);
        helpers.checkElementExistAndSendKeys(protractor.Key.TAB, communityPageObjects.txtSearchQuestion(), 5);
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
        return require('./SearchResults.js');
    };
    this.clickAskAQuestion = function () {
        helpers.checkElementExistAndClick(communityPageObjects.lnkAskAQuestion(), 5);
        return require('../../common/pages/AskQuestion.js');
    };

};
module.exports = new CommunityHome();