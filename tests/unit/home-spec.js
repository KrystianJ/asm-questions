describe('HomePage Functionality', function () {
    beforeEach(function () {
        console.log("In Before Hook");
        this.result = true;

    });

    it('Ran Unit Test 1', function () {
        console.log("Ran Unit Test 1");
        expect(this.result).toEqual(true);

    });
    afterEach(function () {
        console.log("In After Hook");
    });
});